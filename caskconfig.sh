#!/bin/sh

# Essentials
brew cask install alfred
brew cask install onepassword
brew cask install google-chrome
brew cask install firefox
brew cask install spectacle
brew cask install bartender
brew cask install evernote
brew cask install istat-menus
brew cask install mou
brew cask install dropbox
brew cask install google-drive

# Personal
brew cask install spotify
brew cask install vlc
brew cask install ynab
brew cask install scrivener
brew cask install omnifocus

# Development
brew cask install iterm2
brew cask install phpstorm
brew cask install sequel-pro
brew cask install mysqlworkbench
brew cask install sourcetree
brew cask install dash
brew cask install chefdk

# Plugins etc.
brew cask install adobe-air
brew cask install silverlight

# Big apps
brew cask install microsoft-office
brew cask install adobe-creative-cloud && open "/opt/homebrew-cask/Caskroom/adobe-creative-cloud/latest/Creative Cloud Installer.app"

# More serious stuffs
brew cask install virtualbox
brew install adobe-air-sdk